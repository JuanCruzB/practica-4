# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 11:10:58 2021

@author: Administrador
"""
def fact(n):
    if n==1:
        return 1
    else:
        return n*fact(n-1)
    
fact(6)

def fib(n):
    if n==0:
        return 0
    elif n==1:
        return 1
    elif n==2:
        return 1
    else:
        return fib(n-1)+fib(n-2)
    
fib(8)
fib(11)
fib(12)

def sumatoriaPotenciasDeDos(n1,n2):
    n=n2
    if n==n1:
        return n1**2
    else:
        n-=1
        return (n+1)**2+sumatoriaPotenciasDeDos(n1,n)
    
sumatoriaPotenciasDeDos(2, 6)

def sumaImpares(n):
    if n==1:
        return 1
    elif n%2==0:
        n-=1
        return sumaImpares(n)
    elif n%2==1:
        return n+sumaImpares(n-1)
        
sumaImpares(9)
sumaImpares(11)


def sumaDigitos(n):
    if n<10:
        suma=n
    else:
        suma = n%10+ sumaDigitos(n//10)
    return suma
    
sumaDigitos(56)
sumaDigitos(5666)


#def sumaDivisores(n):
 #   if 
    
def divisiblePor3(n):
        if n==3:
            return True
        elif n==6:
            return True
        elif n==9:
            return True
        elif n>11:
            return divisiblePor3(sumaDigitos(n))
        else:
            return False

divisiblePor3(12)
divisiblePor3(27)
divisiblePor3(1332)
divisiblePor3(11)
divisiblePor3(28)
divisiblePor3(1333)
divisiblePor3(8)

def divisiblePor17(n):
    if abs(n)<17:
        return False
    elif abs(n)==17:
        return True
    elif abs(n)>17:
        num=str(n)
        ult=int(num[len(num)-1])*5
        rest=list(num)
        rest=rest[:-1]
        resto=""
        resto=(resto.join(rest))
        resto=int(resto)
        queda=resto-ult
        return divisiblePor17(queda)
    else:
        return False
        
divisiblePor17(935)
divisiblePor17(936)

## Ejercicio 2

def suma(a):
    if len(a)<=1:
        return a[0]
    else:
        return (a[len(a)-1])+(suma(a[:-1]))
    
    
lista=[1,2,3,4,5]
lista.remove(lista[0])

suma(lista)

def maximo(a):
    if len(a)==1:
        return a[0]
    else:
        if a[0]<a[1]:
            a.remove(a[0])
            return maximo(a)
        else:
            a.remove(a[1])
            return maximo(a)
            
listamax=[3,5,8,2,4]

maximo(listamax)

#def promedio(a):
 #   if len(a)==1:
  #      sumita=a[0]
   # else:
    #    sumita=a[len(a)-1]+promedio(a[:-1])
    #return sumita/len(a)
    
def promedio(a):
    return suma(a)/len(a)
    
promedio(listamax)

lista5=[2,6,-1,-3,4,-5,-7,2]

def listaDeAbs(a):
    if len(a)==0:
        return []
    elif len(a)==1:
        res=[abs(a[0])]
        return res
    elif len(a)>1:
        return listaDeAbs(a[:-1]) + [abs(a[len(a)-1])]

listaDeAbs(lista5)

def maximoAbsoluto(a):
    if len(a)==1:
        return abs(a[0])
    else:
        if abs(a[0])<abs(a[1]):
            a.remove(a[0])
            return maximoAbsoluto(a)
        else:
            a.remove(a[1])
            return maximoAbsoluto(a)
        
maximoAbsoluto(lista5)

def cambioDeBase(n,x):
    if n/x<x:
        return [n/x]
    else:
        listita=cambioDeBase(n/x, x)
        elem= n%x
        res = listita + [elem]
        return res
    
cambioDeBase(256, 2)
cambioDeBase(256, 4)
    
def cantidadApariciones(a,x):
    if len(a)==1 and a[0]==x:
        return len(a)
    elif len(a)==2 and a[0]==a[1]==x:
        return len (a)
    elif a[len(a)-1]!=x:
        return cantidadApariciones(a[:-1], x)
    elif a[len(a)-1]==x:
        return 1 + (cantidadApariciones(a[:-1], x))
    
lista6=[2,6,-1,-3,2,2,4,-5,-7,2]


print(cantidadApariciones(lista6, 2))

#def eliminar(a, i):
#    a.remove(a[i])
#    return a

def eliminar(a, i):
    if len(a)==i+1:
        a.remove(a[i])
        return a
    else:
        listafinal = eliminar(a[:-1], i)+ [a[len(a)-1]]
        return listafinal

lista5=[2,6,-1,-3,4,-5,-7,2]

print(eliminar(lista5, 3))

def buscarYEliminar (a, x):
    if len(a)==1:
        if a[0]==x:
            return []
        else:
            return a
    elif len(a)>1:
        if a[len(a)-1]==x:
            return buscarYEliminar(a[:-1], x)
        else:
            return buscarYEliminar(a[:-1], x) + [a[len(a)-1]]
            
lista5=[2,6,-1,-3,4,-5,-7,2]

print(buscarYEliminar(lista5, 2))
        
    
def todosPares(a):
    if len(a)==1:
        if a[0]%2==0:
            return True
        else:
            return False
    else:
        if a[len(a)-1]%2==0:
            return todosPares(a[:-1])
        else:
            return False
        
listapares=[2,4,6,88]
listaconimpares=[2,4,6,87,10]

todosPares(listapares)
todosPares(listaconimpares)

def ordenAscendente(a):
    if len(a)==1:
        return True
    else:
        if a[len(a)-1]>a[len(a)-2]:
            return ordenAscendente(a[:-1])
        else:
            return False
        
ordenAscendente(listapares)
ordenAscendente(listaconimpares)

def reverso(a):
    if len(a)==1:
        lista=[a[0]]
    else:
        lista=[a[len(a)-1]]+(reverso(a[:-1]))
    return lista
        
reverso(listapares)

def sumaPosImpares(a):
    if len(a)<2:
        return 0
    elif len(a)==2:
        return a[1]
    elif len(a)>2:
        suma=a[1]+sumaPosImpares(a[2:])
        return suma

posimp=[3,5,4,3,2]
sumaPosImpares(posimp)

def triangular(a):
    if len(a)==1:
        return True
    elif len(a)>1:
        if a[1]>=a[0]:
            return triangular(a[1:])
        else:
            return False
        
l1=[1,2,3,3,3, 93, 7]
l2=[1,2,1,2]
l3=[1]

triangular(l1)
triangular(l2)
triangular(l3)

## EJERCICIO 3

def mapa(fn, a):
    if len(a)==0:
        lista =[]
    if len(a)==1:
        return [fn(a[0])]
    else:
        lista=[fn(a[0])] + (mapa(fn, a[1:]))
        return lista

mapa(fact, l1)

def Filter(fn, a):
    if len(a)==1:
        if fn(a[0]):
            return [a[0]]
        else:
            return []
    else:
        if fn(a[0]):
            lista=[a[0]] + Filter(fn, a[1:])
            return lista
        else:
            return Filter(fn, a[1:])

Filter(divisiblePor3, l1)

## EJERCICIO 4

def signo(a):
    if a>0:
        return 1
    elif a<0:
        return -1

def A(a, j):
        if j>0:
            suma=signo(a[j])*abs(a[j]**j)+A(a, j-1)
            return suma
        else:
            return signo(a[j])

lista5=[2,6,-1,-3,4,-5,-7,2]

A(lista5, 5)

#B
def primo(n):
    """
    Toma un número y determina si es primo
    """
    i=1
    x=[]
    primalidad=True
    while i<n:
        if n%i==0:
            x.append(i)
        i=i+1
    if len(x)>=2:
        primalidad=False
    return primalidad

primo(66)
primo(67)
primo(444)
primo(953)
def B(n1,n2):
    #devuelve el  menor primo entre  n1 y n2
    if primo(n1) and n1<n2:
        return n1
    elif n1==n2:
        return "No hay primo entre estos dos números"
    else:
        return B(n1+1, n2)
    
B(66, 444)
B(65, 66)

#C
